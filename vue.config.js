module.exports = {
  lintOnSave: false,
  css: {
    loaderOptions: {
      postcss: {
        plugins: [require("tailwindcss")],
      },
    },
  },
  pluginOptions: {
    electronBuilder: {
      nodeIntegration: true,
      // preload: "src/preload.js",
    },
  },
};
