import Vue from "vue";
import VueRouter from "vue-router";
// import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    // component: Home,
    redirect: "/drag",
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: "/chart",
    name: "Chart",
    component: () =>
      import(/* webpackChunkName: "chart" */ "../views/Chart.vue"),
  },
  // {
  //   path: "/boa",
  //   name: "Boa",
  //   component: () => import(/* webpackChunkName: "boa" */ "../views/Boa.vue"),
  // },
  {
    path: "/plot",
    name: "Plot",
    component: () => import(/* webpackChunkName: "plot" */ "../views/Plot.vue"),
  },
  {
    path: "/gl",
    name: "GL",
    component: () => import(/* webpackChunkName: "gl" */ "../views/GL.vue"),
  },
  {
    path: "/plotly",
    name: "Plotly",
    component: () =>
      import(/* webpackChunkName: "plotly" */ "../views/Plotly.vue"),
  },
  {
    path: "/drag",
    name: "Drag",
    component: () => import(/* webpackChunkName: "drag" */ "../views/Drag.vue"),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
