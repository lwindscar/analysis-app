function isObject(object) {
  return object !== null && typeof object === "object";
}

function cloneDeep(object) {
  return JSON.parse(JSON.stringify(object));
}

function get(object, path) {
  return path.reduce((obj, key) => obj[key], object);
}

function pick(object, props) {
  return props.reduce((obj, prop) => ({ ...obj, [prop]: object[prop] }), {});
}

function createUndoModule(setState, options) {
  const { limit = 100 } = options;
  let jumping = false;
  return {
    namespaced: true,
    state: () => ({
      snapshots: [],
      currentIndex: -1,
    }),
    getters: {
      undoList(state) {
        return state.snapshots.slice(0, state.currentIndex);
      },
      redoList(state) {
        return state.snapshots.slice(state.currentIndex + 1);
      },
      canUndo(state) {
        return state.currentIndex > 0;
      },
      canRedo(state) {
        return state.currentIndex < state.snapshots.length - 1;
      },
    },
    mutations: {
      SET_SNAPSHOTS(state, payload) {
        state.snapshots = payload;
      },
      SET_CURRENT_INDEX(state, payload) {
        state.currentIndex = payload;
      },
    },
    actions: {
      insert({ state, commit }, snapshot) {
        if (jumping) {
          return;
        }
        const sliceEnd = state.currentIndex + 1;
        const sliceStart = sliceEnd >= limit ? 1 : 0;
        const sliceList = state.snapshots.slice(sliceStart, sliceEnd);

        sliceList.push(cloneDeep(snapshot));

        commit("SET_SNAPSHOTS", sliceList);
        commit("SET_CURRENT_INDEX", sliceList.length - 1);
      },
      jump({ state, commit }, step) {
        const newCurrentIndex = state.currentIndex + step;
        if (
          step === 0 ||
          newCurrentIndex < 0 ||
          newCurrentIndex > state.snapshots.length - 1
        ) {
          return;
        }

        jumping = true;
        commit("SET_CURRENT_INDEX", newCurrentIndex);
        const snapshot = cloneDeep(state.snapshots[newCurrentIndex]);
        setState(snapshot, () => {
          jumping = false;
        });
      },
      clear({ commit }) {
        commit("SET_SNAPSHOTS", []);
        commit("SET_CURRENT_INDEX", -1);
      },
      undo({ dispatch }) {
        return dispatch("jump", -1);
      },
      redo({ dispatch }) {
        return dispatch("jump", 1);
      },
    },
  };
}

const plugin = store => {
  const modulesNamespaceMap = store._modulesNamespaceMap;
  Object.keys(modulesNamespaceMap).forEach(namespace => {
    const module = modulesNamespaceMap[namespace];
    const { _rawModule, state: initialState, _children } = module;
    const { undo } = _rawModule;
    if (!undo) {
      return;
    }

    const initialKeys = Object.keys(initialState);
    const childKeys = Object.keys(_children);
    const path = namespace.replace(/\/$/, "").split("/");

    const options = isObject(undo) ? undo : {};
    const { setStateMutation = "setState", include, exclude } = options;
    const setState = (newState, cb) => {
      store.commit(`${namespace}${setStateMutation}`, newState);
      store._vm.$nextTick(cb);
    };
    const undoModule = createUndoModule(setState, options);
    const undoModuleName = "$undo";
    store.registerModule([...path, undoModuleName], undoModule);

    const watchedKeys = initialKeys.filter(key => {
      if (Array.isArray(include)) {
        return include.includes(key);
      }
      if (Array.isArray(exclude)) {
        return !exclude.includes(key);
      }
      if (childKeys.length > 0) {
        return !childKeys.includes(key);
      }
      return true;
    });
    store.watch(
      state => pick(get(state, path), watchedKeys),
      snapshot => {
        store.dispatch(`${namespace}${undoModuleName}/insert`, snapshot);
      },
      { deep: false, immediate: false }
    );
  });
};

export default plugin;
