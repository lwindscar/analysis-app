function isObject(object) {
  return object !== null && typeof object === "object";
}

function cloneDeep(object) {
  return JSON.parse(JSON.stringify(object));
}

function pick(object, props) {
  return props.reduce((obj, prop) => ({ ...obj, [prop]: object[prop] }), {});
}

export function install(Vue) {
  if (install.installed || !Vue) {
    return;
  }

  function createUndo(vm, options) {
    const {
      limit = 20,
      include,
      // exclude,
    } = options;
    let jumping = false;
    return new Vue({
      data() {
        return {
          snapshots: [],
          currentIndex: -1,
        };
      },
      computed: {
        undoList() {
          const snapshots = this.snapshots.slice(0, this.currentIndex);
          const list = snapshots.map((snapshot, index) => ({
            snapshot,
            snapshotIndex: index,
          }));
          list.reverse();
          return list;
        },
        redoList() {
          const sliceStart = this.currentIndex + 1;
          const snapshots = this.snapshots.slice(sliceStart);
          const list = snapshots.map((snapshot, index) => ({
            snapshot,
            snapshotIndex: sliceStart + index,
          }));
          return list;
        },
        canUndo() {
          return this.currentIndex > 0;
        },
        canRedo() {
          return this.currentIndex < this.snapshots.length - 1;
        },
      },
      methods: {
        insert(snapshot) {
          if (jumping) {
            return;
          }
          const sliceEnd = this.currentIndex + 1;
          const sliceStart = sliceEnd >= limit ? 1 : 0;
          const sliceList = this.snapshots.slice(sliceStart, sliceEnd);

          sliceList.push(cloneDeep(snapshot));

          this.snapshots = sliceList;
          this.currentIndex = sliceList.length - 1;
        },
        jump(snapshotIndex) {
          if (snapshotIndex < 0 || snapshotIndex > this.snapshots.length - 1) {
            return;
          }

          jumping = true;
          this.currentIndex = snapshotIndex;
          const snapshot = this.snapshots[snapshotIndex];
          Object.keys(snapshot).forEach(key => {
            vm[key] = snapshot[key];
          });
          vm.$nextTick(() => {
            jumping = false;
          });
        },
        clear() {
          this.snapshots = [];
          this.currentIndex = -1;
        },
        go(step) {
          const newCurrentIndex = this.currentIndex + step;
          this.jump(newCurrentIndex);
        },
        undo() {
          this.go(-1);
        },
        redo() {
          this.go(1);
        },
      },
      created() {
        //   const initialKeys = Object.keys(vm._data);
        //   const watchedKeys = initialKeys.filter((key) => {
        //     if (Array.isArray(include)) {
        //       return include.includes(key);
        //     }
        //     if (Array.isArray(exclude)) {
        //       return !exclude.includes(key);
        //     }
        //     return true;
        //   });
        //   vm.$watch(
        //     () => pick(vm, watchedKeys),
        //     (snapshot) => {
        //       this.insert(snapshot);
        //     },
        //     { deep: true, immediate: true }
        //   );
        const computedFn = include ? () => pick(vm, include) : () => this._data;
        vm.$watch(
          computedFn,
          snapshot => {
            this.insert(snapshot);
          },
          { deep: true, immediate: true }
        );
      },
    });
  }

  Vue.mixin({
    created() {
      const { undo } = this.$options;
      if (!undo) {
        return;
      }

      const options = isObject(undo) ? undo : {};
      this.$undo = createUndo(this, options);
    },
  });

  install.installed = true;
}

export default { install };
