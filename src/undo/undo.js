import Vue from "vue";
import clone from "lodash/clone";
import mapValues from "lodash/mapValues";
import each from "lodash/each";

function undoable(module) {
  const { state: initialState, getters, mutations } = module;

  const history = Vue.observable({
    past: [],
    future: [],
    present: clone(initialState),
  });

  const newMutations = {
    ...mapValues(mutations, function(handler) {
      return function(state, payload) {
        handler.call(this, state, payload);
        const newPresent = clone(state);
        history.past = [...history.past, history.present];
        history.future = [];
        history.present = newPresent;
      };
    }),
    undo(state) {
      if (history.past.length === 0) {
        return;
      }
      const previous = history.past[history.past.length - 1];
      const newPast = history.past.slice(0, history.past.length - 1);
      history.past = newPast;
      history.future = [history.present, ...history.future];
      history.present = previous;
      each(history.present, (v, k) => {
        state[k] = v;
      });
    },
    redo(state) {
      if (history.future.length === 0) {
        return;
      }
      const next = history.future[0];
      const newFuture = history.future.slice(1);
      history.past = [...history.past, history.present];
      history.future = newFuture;
      history.present = next;
      each(history.present, (v, k) => {
        state[k] = v;
      });
    },
  };

  const newGetters = {
    ...getters,
    past() {
      return history.past;
    },
    future() {
      return history.future;
    },
    canUndo() {
      return history.past.length > 0;
    },
    canRedo() {
      return history.future.length > 0;
    },
  };

  return {
    ...module,
    mutations: newMutations,
    getters: newGetters,
    namespaced: true,
  };
}

export default undoable;
