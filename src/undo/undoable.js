import clone from "lodash/clone";
import pick from "lodash/pick";
import mapValues from "lodash/mapValues";
import each from "lodash/each";

const STATE_PAST = "__past";
const STATE_FUTURE = "__future";
const STATE_PRESENT = "__present";

const MUTATION_UNDO = "undo";
const MUTATION_REDO = "redo";
const MUTATION_JUMP = "jump";
const MUTATION_CLEAR_UNDO = "clearUndo";

const GETTER_UNDO_LIST = "undoList";
const GETTER_REDO_LIST = "redoList";
const GETTER_CAN_UNDO = "canUndo";
const GETTER_CAN_REDO = "canRedo";

function insert(state, keys, limit) {
  const { [STATE_PAST]: past, [STATE_PRESENT]: present } = state;

  const lengthWithoutFuture = past.length + 1;
  const isOverflow = limit && limit <= lengthWithoutFuture;
  const pastSliced = past.slice(isOverflow ? 1 : 0);
  const newPast = [...pastSliced, present];

  const newPresent = clone(pick(state, keys));

  state[STATE_PAST] = newPast;
  state[STATE_FUTURE] = [];
  state[STATE_PRESENT] = newPresent;
}

function jumpToPast(state, index) {
  if (index < 0 || index >= state[STATE_PAST].length) {
    return;
  }

  const {
    [STATE_PAST]: past,
    [STATE_FUTURE]: future,
    [STATE_PRESENT]: present,
  } = state;

  const newPast = past.slice(0, index);
  const newFuture = [...past.slice(index + 1), present, ...future];
  const newPresent = past[index];

  state[STATE_PAST] = newPast;
  state[STATE_FUTURE] = newFuture;
  state[STATE_PRESENT] = newPresent;
  each(newPresent, (v, k) => {
    state[k] = v;
  });
}

function jumpToFuture(state, index) {
  if (index < 0 || index >= state[STATE_FUTURE].length) {
    return;
  }

  const {
    [STATE_PAST]: past,
    [STATE_FUTURE]: future,
    [STATE_PRESENT]: present,
  } = state;

  const newPast = [...past, present, ...future.slice(0, index)];
  const newPresent = future[index];
  const newFuture = future.slice(index + 1);

  state[STATE_PAST] = newPast;
  state[STATE_FUTURE] = newFuture;
  state[STATE_PRESENT] = newPresent;
  each(newPresent, (v, k) => {
    state[k] = v;
  });
}

function jump(state, n) {
  if (n > 0) {
    jumpToFuture(state, n - 1);
  }
  if (n < 0) {
    jumpToPast(state, state[STATE_PAST].length + n);
  }
}

function undoable(module, config = {}) {
  const { state: initialState, mutations, getters } = module;

  const {
    limit,
    mutations: mutationTypes = {},
    getters: getterTypes = {},
  } = config;

  const keys = Object.keys(initialState);

  const newState = {
    ...initialState,
    [STATE_PAST]: [],
    [STATE_FUTURE]: [],
    [STATE_PRESENT]: clone(initialState),
  };

  const newMutations = {
    ...mapValues(mutations, function(handler) {
      return function(state, payload) {
        handler.call(this, state, payload);
        insert(state, keys, limit);
      };
    }),
    [mutationTypes.undo || MUTATION_UNDO](state) {
      jump(state, -1);
    },
    [mutationTypes.redo || MUTATION_REDO](state) {
      jump(state, 1);
    },
    [mutationTypes.jump || MUTATION_JUMP](state, index) {
      jump(state, index);
    },
    [mutationTypes.clearUndo || MUTATION_CLEAR_UNDO](state) {
      state[STATE_PAST] = [];
      state[STATE_FUTURE] = [];
    },
  };

  const newGetters = {
    ...getters,
    [getterTypes.undoList || GETTER_UNDO_LIST](state) {
      return state[STATE_PAST];
    },
    [getterTypes.redoList || GETTER_REDO_LIST](state) {
      return state[STATE_FUTURE];
    },
    [getterTypes.canUndo || GETTER_CAN_UNDO](state) {
      return state[STATE_PAST].length > 0;
    },
    [getterTypes.canRedo || GETTER_CAN_REDO](state) {
      return state[STATE_FUTURE].length > 0;
    },
  };

  return {
    ...module,
    state: newState,
    mutations: newMutations,
    getters: newGetters,
    namespaced: true,
  };
}

export default undoable;
