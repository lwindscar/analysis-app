function cloneDeep(object) {
  return JSON.parse(JSON.stringify(object));
}

export default class UndoStore {
  constructor(options = {}) {
    this.limit = options.limit || 100;
    this.list = [];
    this.presentIndex = -1;
  }

  insert(state) {
    const sliceEndIndex = this.presentIndex + 1;
    const sliceStartIndex = sliceEndIndex >= this.limit ? 1 : 0;
    const sliceList = this.list.slice(sliceStartIndex, sliceEndIndex);

    sliceList.push(cloneDeep(state));

    this.list = sliceList;
    this.presentIndex = sliceList.length - 1;
  }

  jump(step) {
    const newPresentIndex = this.presentIndex + step;
    if (
      step === 0 ||
      newPresentIndex < 0 ||
      newPresentIndex > this.list.length - 1
    ) {
      return;
    }

    this.presentIndex = newPresentIndex;
    return cloneDeep(this.list[newPresentIndex]);
  }

  clear() {
    this.list = [];
    this.presentIndex = -1;
  }

  undo() {
    return this.jump(-1);
  }

  redo() {
    return this.jump(1);
  }

  get undoList() {
    return this.list.slice(0, this.presentIndex);
  }

  get redoList() {
    return this.list.slice(this.presentIndex + 1);
  }

  get canUndo() {
    return this.presentIndex > 0;
  }

  get canRedo() {
    return this.presentIndex < this.list.length - 1;
  }
}

// class UndoStore {
//   constructor(options = {}) {
//     this.limit = options.limit || 100;
//     this.vm = options.vm;
//     // this.list = [];
//     // this.presentIndex = -1;
//     this.$$ = Vue.observable({
//       list: [],
//       presentIndex: -1,
//     });

//     this.jumping = false;
//     this.vm.$watch("$data", {
//       handler: (data) => {
//         if (this.jumping) {
//           return;
//         }
//         this.insert(data);
//       },
//       deep: true,
//       immediate: true,
//     });
//   }

//   insert(state) {
//     const sliceEndIndex = this.$$.presentIndex + 1;
//     const sliceStartIndex = sliceEndIndex >= this.limit ? 1 : 0;
//     const sliceList = this.$$.list.slice(sliceStartIndex, sliceEndIndex);

//     sliceList.push(cloneDeep(state));

//     this.$$.list = sliceList;
//     this.$$.presentIndex = sliceList.length - 1;
//   }

//   jump(step) {
//     if (step === 0) {
//       return;
//     }
//     if (step > 0 && this.$$.presentIndex + step > this.$$.list.length - 1) {
//       return;
//     }
//     if (step < 0 && this.$$.presentIndex + step < 0) {
//       return;
//     }

//     this.jumping = true;
//     this.$$.presentIndex = this.$$.presentIndex + step;
//     const state = cloneDeep(this.$$.list[this.$$.presentIndex]);
//     Object.keys(state).forEach((key) => {
//       this.vm[key] = state[key];
//     });
//     this.vm.$nextTick(() => {
//       this.jumping = false;
//     });
//   }

//   clear() {
//     this.$$.list = [];
//     this.$$.presentIndex = -1;
//   }

//   undo() {
//     return this.jump(-1);
//   }

//   redo() {
//     return this.jump(1);
//   }

//   get undoList() {
//     return this.$$.list.slice(0, this.$$.presentIndex);
//   }

//   get redoList() {
//     return this.$$.list.slice(this.$$.presentIndex + 1);
//   }

//   get canUndo() {
//     return this.$$.presentIndex > 0;
//   }

//   get canRedo() {
//     return this.$$.presentIndex < this.$$.list.length - 1;
//   }
// }
