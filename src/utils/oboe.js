import oboe from "oboe";

const large_json_url =
  "https://raw.githubusercontent.com/RandomFractals/ChicagoCrimes/master/data/2018/chicago-crimes-2018.json";

oboe(large_json_url)
  .node("$!.*", function(things, path, ancestors) {
    // This callback will be called everytime a new object is
    // found in the foods array.

    console.log("things", things);

    console.log("path: ", path);

    console.log("ancestors: ", ancestors);

    // 每500条数据更新一下页面
    // if (path[0] / 500 === 0) {
    //   // update UI
    // }
  })
  .node("![9]", function() {
    console.log("root:", this.root());
    this.abort();
  })
  // .node("3DUMAP", function(things) {
  //   // This callback will be called everytime a new object is
  //   // found in the foods array.

  //   console.log("3DUMAP1", things);
  // })
  // .on("10", function(things) {
  //   // This callback will be called everytime a new object is
  //   // found in the foods array.

  //   console.log("10", things);
  // })
  // .node("cellID", function(things) {
  //   console.log("cellID", things);
  // })
  .done(function(things) {
    console.log("done: ", things);
  })
  .fail(function(error) {
    console.log("error: ", error);
  });
