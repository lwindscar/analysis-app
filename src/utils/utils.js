import _ from "lodash";
import json from "./data.json";

export function getPaginatedList(array, page, pageSize) {
  const obj = {};
  const start = (page - 1) * pageSize;
  const end = page * pageSize;

  obj.data = array.slice(start, end);
  obj.total = array.length;
  obj.page = page;
  obj.pageSize = pageSize;

  return obj;
}

export const readFileAsText = file => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onerror = reject;
    reader.onload = () => {
      resolve(event.target.result);
    };
    reader.readAsText(file);
  });
};

export const mergeTableSpan = (tableData, childrenKey = "children") => {
  return _.flatMap(tableData, item => {
    const { [childrenKey]: children = [], ...rest } = item;
    if (_.isEmpty(children)) {
      return {
        ...rest,
        rowspan: 1,
      };
    }
    return _.map(children, (child, index) => {
      const rowspan = index === 0 ? _.size(children) : 0;
      return {
        ...child,
        ...rest,
        rowspan,
      };
    });
  });
};

export const downloadBlob = (blob, filename = "untitled") => {
  if (typeof window.navigator.msSaveBlob !== "undefined") {
    window.navigator.msSaveBlob(blob, filename);
  } else {
    const blobURL = window.URL.createObjectURL(blob);
    const tempLink = document.createElement("a");
    tempLink.style.display = "none";
    tempLink.href = blobURL;
    tempLink.setAttribute("download", filename);

    if (typeof tempLink.download === "undefined") {
      tempLink.setAttribute("target", "_blank");
    }

    document.body.appendChild(tempLink);
    tempLink.click();
    document.body.removeChild(tempLink);
    window.URL.revokeObjectURL(blobURL);
  }
};

export const downloadURL = (url, filename) => {
  fetch(url)
    .then(res => res.blob())
    .then(blob => downloadBlob(blob, filename));
};

export const downloadJSON = (data, filename = "data") => {
  const json = JSON.stringify(data, undefined, 2);
  const blob = new Blob([json], { type: "text/json" });
  downloadBlob(blob, `${filename}.json`);
};

export const delay = (ms = 300, value) => {
  return new Promise(resolve => {
    setTimeout(() => resolve(value), ms);
  });
};

export const loadData = async () => {
  await delay(1000);
  return json;
};
