import boa from "@pipcook/boa";

// const os = boa.import("os");
// console.log(os.getpid()); // prints the pid from python.

// const { range, len } = boa.builtins();
// const list = range(0, 10); // create a range array
// console.log("list: ", list);
// console.log(len(list)); // 10
// console.log(list[2]); //

// const np = boa.import("numpy");
// const x = np.arange(15).reshape(3, 5);
// console.log("x: ", x);
// console.log("dim: ", x.ndim);
// console.log("size: ", x.size);

// const pd = boa.import("pandas");
// const s = pd.Series([1, 3, 5, np.nan, 6, 8]);
// console.log("s: ", s.toString());
// console.log("s head:", s.head().toString());

export const readH5 = filePath => {
  const sc = boa.import("scanpy");

  const adata = sc.read_10x_h5(filePath);

  // console.log(adata);

  const o = adata.obs.toString();
  // console.log("obs: ", o);

  const x = adata.X.toString();
  // console.log("X: ", x);

  const v = adata.var.toString();
  // console.log("var: ", v);

  return { o, x, v };
};
