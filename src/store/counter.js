import undoable from "./undoable";

export default undoable({
  state: {
    count: 0,
  },
  getters: {},
  mutations: {
    increase(state, num) {
      state.count += num;
    },
    decrease(state) {
      state.count -= 1;
    },
  },
  actions: {},
});
