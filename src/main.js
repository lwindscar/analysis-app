// import "ress";
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VuePlotly from "./components/VuePlotly";
import "./plugins/ant-design-vue.js";
import logger from "electron-log";

Vue.config.productionTip = false;

Vue.component("v-plot", VuePlotly);

Vue.prototype.$logger = logger;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount("#app");
